// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.19.4
// source: type.proto

package _type

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	Type_SearchType_FullMethodName = "/type.type/SearchType"
)

// TypeClient is the client API for Type service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type TypeClient interface {
	SearchType(ctx context.Context, in *SearchTypeRequest, opts ...grpc.CallOption) (*SearchTopResponse, error)
}

type typeClient struct {
	cc grpc.ClientConnInterface
}

func NewTypeClient(cc grpc.ClientConnInterface) TypeClient {
	return &typeClient{cc}
}

func (c *typeClient) SearchType(ctx context.Context, in *SearchTypeRequest, opts ...grpc.CallOption) (*SearchTopResponse, error) {
	out := new(SearchTopResponse)
	err := c.cc.Invoke(ctx, Type_SearchType_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// TypeServer is the server API for Type service.
// All implementations must embed UnimplementedTypeServer
// for forward compatibility
type TypeServer interface {
	SearchType(context.Context, *SearchTypeRequest) (*SearchTopResponse, error)
	mustEmbedUnimplementedTypeServer()
}

// UnimplementedTypeServer must be embedded to have forward compatible implementations.
type UnimplementedTypeServer struct {
}

func (UnimplementedTypeServer) SearchType(context.Context, *SearchTypeRequest) (*SearchTopResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchType not implemented")
}
func (UnimplementedTypeServer) mustEmbedUnimplementedTypeServer() {}

// UnsafeTypeServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to TypeServer will
// result in compilation errors.
type UnsafeTypeServer interface {
	mustEmbedUnimplementedTypeServer()
}

func RegisterTypeServer(s grpc.ServiceRegistrar, srv TypeServer) {
	s.RegisterService(&Type_ServiceDesc, srv)
}

func _Type_SearchType_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchTypeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TypeServer).SearchType(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: Type_SearchType_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TypeServer).SearchType(ctx, req.(*SearchTypeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Type_ServiceDesc is the grpc.ServiceDesc for Type service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Type_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "type.type",
	HandlerType: (*TypeServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SearchType",
			Handler:    _Type_SearchType_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "type.proto",
}
