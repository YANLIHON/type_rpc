package main

import (
	"flag"
	"fmt"
	"gitee.com/YANLIHON/type_rpc/internal/config"
	"gitee.com/YANLIHON/type_rpc/internal/model"
	"gitee.com/YANLIHON/type_rpc/internal/server"
	"gitee.com/YANLIHON/type_rpc/internal/svc"
	_type "gitee.com/YANLIHON/type_rpc/type"
	"github.com/BarnabyCharles/frame/app"
	config2 "github.com/BarnabyCharles/frame/config"
	"github.com/BarnabyCharles/frame/databases/mysql"
	"github.com/ghodss/yaml"
	"log"

	"github.com/zeromicro/go-zero/core/service"
	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	flag.Parse()
	var c config.Config
	//连接配置
	viper, err := config2.InitViper("etc", "./etc/type.yaml", "yaml")
	if err != nil {
		log.Println(err)
		return
	}

	err = app.Init(viper.GetString("Nacos.serverName"),
		viper.GetString("Nacos.group"),
		viper.GetString("Nacos.namespaceId"),
		viper.GetString("Nacos.host"),
		viper.GetInt("Nacos.port"), "mysql")
	if err != nil {
		log.Println(err)
		return
	}

	nacosConfig, err := config2.GetNacosConfig(viper.GetString("Nacos.serverName"), viper.GetString("Nacos.group"))
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal([]byte(nacosConfig), &c)
	if err != nil {
		panic(err)
	}
	ctx := svc.NewServiceContext(c)
	//创建表
	model.NewAutoMigrate()
	if err != nil {
		fmt.Println(err)
		return
	}
	db, err := mysql.DB.DB()

	defer db.Close()

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		_type.RegisterTypeServer(grpcServer, server.NewTypeServer(ctx))

		if c.Mode == service.DevMode || c.Mode == service.TestMode {
			reflection.Register(grpcServer)
		}
	})
	defer s.Stop()

	fmt.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
