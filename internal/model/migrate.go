package model

import (
	"fmt"
	"github.com/BarnabyCharles/frame/databases/mysql"
)

func NewAutoMigrate() {
	classify := NewType()

	err := mysql.DB.AutoMigrate(classify)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}
