package model

import (
	"github.com/BarnabyCharles/frame/databases/mysql"
	"gorm.io/gorm"
)

type Type struct {
	gorm.Model
	Title    string `gorm:"type:varchar(100)"`
	Url      string `gorm:"INDEX,type:text"`
	ParentID int64  `gorm:"type:int"`
	Region   int64  `gorm:"type:tinyint(3)"`
	Part     int64  `gorm:"type:tinyint(3)"`
}

func NewType() *Type {
	return new(Type)
}

type ResTypeInfo struct {
	Id       int64  `json:"id"`
	Title    string `json:"title"`
	Url      string `json:"url"`
	Image    string `json:"image"`
	ParentID int64  `json:"parent_id"`
}

// 查询对应位置的数据
func (c *Type) SearchTypeInfo(region, part int64) ([]ResTypeInfo, error) {
	Type := NewType()
	res := []ResTypeInfo{}
	err := mysql.DB.Model(Type).Where("region = ?", region).Where("part = ?", part).Find(&res).Error

	if err != nil {
		return nil, err
	}
	return res, nil
}
