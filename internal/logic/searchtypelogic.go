package logic

import (
	"context"
	"gitee.com/YANLIHON/type_rpc/internal/model"
	"gitee.com/YANLIHON/type_rpc/internal/svc"
	_type "gitee.com/YANLIHON/type_rpc/type"

	"github.com/zeromicro/go-zero/core/logx"
)

type SearchTypeLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSearchTypeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SearchTypeLogic {
	return &SearchTypeLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SearchTypeLogic) SearchType(in *_type.SearchTypeRequest) (*_type.SearchTopResponse, error) {

	typeInfo := model.NewType()
	infos, err := typeInfo.SearchTypeInfo(in.Region, in.Part)
	if err != nil {
		return nil, err
	}
	resInfos := []*_type.ResInfo{}

	for _, val := range infos {
		conInfo := &_type.ResInfo{
			Id:       val.Id,
			Title:    val.Title,
			Image:    val.Image,
			ParentID: val.ParentID,
		}

		resInfos = append(resInfos, conInfo)
	}
	return &_type.SearchTopResponse{ResInfos: resInfos}, nil
}
